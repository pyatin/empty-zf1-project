<?php


defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__)));

defined('APPLICATION_ENV') || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

defined('PUBLIC_PATH') || define('PUBLIC_PATH', realpath(APPLICATION_PATH . '/../public'));

define('LIBRARY_PATH', realpath(APPLICATION_PATH . '/../lib'));

define('CONFIGS_PATH', APPLICATION_PATH . '/configs');

define('MODELS_PATH', APPLICATION_PATH . '/models');

define('FORMS_PATH', APPLICATION_PATH . '/forms');

set_include_path(implode(PATH_SEPARATOR, array(
    LIBRARY_PATH,
    APPLICATION_PATH,
    MODELS_PATH,
    FORMS_PATH,
    get_include_path(),
)));

require_once 'Zend/Loader/Autoloader.php';

$loader = Zend_Loader_Autoloader::getInstance();
$loader->setFallbackAutoloader(true);

$defaultLoader = new Zend_Application_Module_Autoloader(array(
    'namespace' => '',
    'basePath' => APPLICATION_PATH
));
