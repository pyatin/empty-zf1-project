<?php

class ErrorController extends Zend_Controller_Action
{

    public function errorAction()
    {
        $this->_helper->layout()->disableLayout();

        $errors = $this->_getParam('error_handler');
        $message = '';
        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $message = "The page you requested was not found.";
                break;
            default:
                // application error
                $this->getResponse()->setHttpResponseCode(500);
                $message =
                    'An unexpected error occurred with your request. ' .
                    'Please try again later.';

                break;
        }

        $params = Zend_Controller_Front::getInstance()->getParams();
  

        $this->view->message   = $message;

        if($params['displayExceptions']) {
            $this->view->exception = $errors->exception;
            $this->view->request   = $errors->request;
        }
        
    }
}