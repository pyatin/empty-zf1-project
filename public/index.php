<?php

require_once realpath(dirname(__FILE__) . '/../app/boot.php');

$application = new Zend_Application(
    APPLICATION_ENV,
    CONFIGS_PATH . '/main.ini'
);

$application->bootstrap();
$application->run();
